# Before Developing

> This project depends on having node, npm, and yar. Before developing, be sure to install them.

To install node, use NVM: https://github.com/nvm-sh/nvm

Yarn: https://classic.yarnpkg.com/en/docs/install/#mac-stable

1. create your `.env'

```sh
echo "BROWSER=NONE" >> .env
```

2. install dependencies

```sh
yarn install
```

3. to develop:

```sh
yarn run dev
```
