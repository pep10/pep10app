import React, { useState, createContext } from "react";

// Context for icon highlighting
export const SelectedModeContext = createContext("");

// Context provider for icon highlighting
export const SelectedModeContextProvider = ({ children }) => {
  // must be one of `code`, `bug` or `help`
  const [selected, setSelected] = useState("code");

  return (
    <SelectedModeContext.Provider value={{ selected, setSelected }}>
      {children}
    </SelectedModeContext.Provider>
  );
};
