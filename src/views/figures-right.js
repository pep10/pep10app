import React from "react";
import styled from "styled-components";

import { ThreeToOne } from "../templates";

/**
 * TODO: Better name and real components
 * The right column
 */
export const Figures = () => {
  return (
    <ThreeToOne>
      <ComponentWrapper />
      <ComponentWrapper />
    </ThreeToOne>
  );
};

const ComponentWrapper = styled.div`
  background-color: purple;
`;
