import React from "react";
import styled from "styled-components";

import { EvenFlex } from "../templates";

/**
 * TODO: Better name and real components
 * The left column when `/help` is selected
 */
export const HelpLeft = () => {
  return (
    <EvenFlex width={300}>
      <ComponentWrapper />
      <ComponentWrapper />
    </EvenFlex>
  );
};

const ComponentWrapper = styled.div`
  background-color: purple;
`;
