import React, { useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCode,
  faQuestionCircle,
  faBug,
} from "@fortawesome/free-solid-svg-icons";

import styled from "styled-components";
import { Link } from "@reach/router";

import { SelectedModeContext } from "../contexts";

// Data source for the icons
const icons = [
  { title: "code", icon: faCode },
  { title: "bug", icon: faBug },
  { title: "help", icon: faQuestionCircle },
];

/**
 * The navigation bar for the application
 */
const IconBar = () => {
  // Context for determining which icon to highlight
  const { selected, setSelected } = useContext(SelectedModeContext);

  /**
   * Callback handler for selecting and icon
   *
   * @param {String} title The title of the selected icon. This title
   *                       must match one of the titles defined in the
   *                       data source
   */
  const iconPressed = (title) => {
    setSelected(title);
  };

  return (
    <Wrapper>
      {icons.map(({ title, icon }) => (
        <Link
          to={`/${title === "code" ? "" : title}`}
          onClick={() => iconPressed(title)}
        >
          <ContentWrapper>
            <HighlightBar selected={title === selected} />
            <IconWrapper>
              <Icon selected={title === selected} className="fas" icon={icon} />
            </IconWrapper>
          </ContentWrapper>
        </Link>
      ))}
    </Wrapper>
  );
};

export default IconBar;

const Wrapper = styled.div`
  background-color: var(--grayLight);
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: stretch;
`;

const HighlightBar = styled.div`
  background-color: ${({ selected }) => (selected ? "var(--white)" : "none")};
  width: 2px;
`;

const ContentWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 32px;

  &:hover {
    cursor: pointer;
    color: var(--white);
  }
`;

const IconWrapper = styled.div`
  display: flex;
  margin-right: 16px;
`;

const Icon = styled(FontAwesomeIcon)`
  height: 40px;
  min-width: 30px;
  color: ${({ selected }) =>
    selected ? "var(--white)" : "var(--iconDisabled)"};

  &:hover {
    cursor: pointer;
    color: var(--white);
  }
`;
