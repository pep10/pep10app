import React, { useState } from "react";

import styled from "styled-components";

// TODO: The styling on the tabs for layingout isn't the best
//       it will need to be reworked

/**
 * A tab component to be used with `Tabs`. This component
 * is not exported and is for internal `Tabs`-use only
 *
 * @param {object} props The props for the component
 * @param {string} title The tab title
 * @param {string} selectedTab The currently displayed tab
 * @param {function} tabItemClicked The callback called when the tab is selected
 */
const Tab = ({ title, selectedTab, tabItemClicked }) => {
  const tabSelected = () => {
    tabItemClicked(title);
  };

  return (
    <TabItem selected={selectedTab === title} onClick={tabSelected}>
      {title}
    </TabItem>
  );
};

/**
 * Creates a tabview
 *
 * @param {object} props The component props
 * @param {object[]} tabs The data to create tabs from
 * @param {string} tabs.title The title of the tab
 * @param {JSX.Element} tabs.Tab The component to be made into the tab
 */
export const Tabs = ({ tabs }) => {
  const [selectedTab, setSelectedTab] = useState(tabs[0].title);

  const tabItemClicked = (title) => {
    setSelectedTab(title);
  };

  return (
    <>
      <TabList>
        {tabs.map(({ title }) => (
          <Tab
            selectedTab={selectedTab}
            tabItemClicked={tabItemClicked}
            title={title}
          />
        ))}
      </TabList>
      <TabContent>
        {tabs.map(({ title, Tab }) => {
          if (title !== selectedTab) return null;

          return <Tab />;
        })}
      </TabContent>
    </>
  );
};

const TabItem = styled.div`
  background-color: ${({ selected }) =>
    selected ? "var(--grayLight)" : "var(--gray)"};
  color: var(--white);
  display: inline-block;
  list-style: none;
  width: 50%;
  text-align: center;
  padding: 16px 0;
`;

const TabList = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-around;
`;

const TabContent = styled.div`
  background-color: blue;
`;
