import React, { useEffect } from "react";
import styled from "styled-components";
// TODO: Re introduce later
// import Editor from "@monaco-editor/react";
import { setupEditor } from "./editor-setup";
import { EvenFlex } from "../templates";

/**
 * TODO: Better naming and real components
 * PepEditor container
 */
export const PepEditor = () => {
  useEffect(() => {
    (async function () {
      await setupEditor();
    })();
  }, []);

  return (
    <EvenFlex>
      <ComponentWrapper />
      <ComponentWrapper />
      <ComponentWrapper />
    </EvenFlex>
  );
};

const ComponentWrapper = styled.div`
  background-color: purple;
`;
