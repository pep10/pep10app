import { loader } from "@monaco-editor/react";

export const setupEditor = async () => {
  const instance = await loader.init();

  instance.languages.register({ id: "pep10" });

  // Register a tokens provider for the language
  instance.languages.setMonarchTokensProvider("pep10", {
    tokenizer: {
      root: [
        [/\[error.*/, "custom-error"],
        [/\[notice.*/, "custom-notice"],
        [/\[info.*/, "custom-info"],
        [/\[[a-zA-Z 0-9:]+\]/, "custom-date"],
      ],
    },
  });

  // Define a new theme that contains only rules that match this language
  instance.editor.defineTheme("pep10Theme", {
    base: "vs",
    inherit: false,
    rules: [
      { token: "custom-info", foreground: "808080" },
      { token: "custom-error", foreground: "ff0000", fontStyle: "bold" },
      { token: "custom-notice", foreground: "FFA500" },
      { token: "custom-date", foreground: "008800" },
    ],
  });
};
