import { createGlobalStyle } from "styled-components";
import { normalize } from "styled-normalize";

export const ThemeStyle = createGlobalStyle`
${normalize}
:root { 
  --headerFontFamily: sans-serif;
  --bodyFontFamily: sans-serif;
  --bodyFontSize: 16px;
  --smBodyFontSize: 12px;
  --mdBodyFontSize: 16px;
  --lgBodyFontSize: 18px;
  --fwLight: 300;
  --fwRegular: 400;
  --fwMedium: 500;
  --fwBold: 700;

  --gutter: 24px;
  --spacing: 2rem;

  --grayLight: #3a3a3a;
  --gray: #2a2a2b;
  --iconDisabled: #909090;
  --white: #ffffff;
}

html {
  background-color: var(--whiteMain);
  box-sizing: border-box;
  font-size: var(--bodyFontSize);
  margin: 0;
  padding: 0;
}

*,
*:before,
*:after {
  box-sizing: inherit;
}

body {
  color: var(--blackMain);
  font-family: var(--bodyFontFamily);
  font-weight: 400;
  margin: 0;
  padding: 0;
}

h1,
.h1,
h2,
.h2,
h3,
.h3,
h4,
.h4,
h5,
.h5 {
  color: var(--headerColor);
  font-family: var(--headerFontFamily);
  font-weight: var(--fwRegular);
  line-height: 1.2em;
  padding: 0;
  margin: var(--spacing) 0 0;
}

h1,
.h1 {
font-size: 3.052rem;
  letter-spacing: -1.5px;
}

h2,
.h2 {
  font-size: 2.441rem;
  letter-spacing: -0.5px;
}

h3,
.h3 {
  font-size: 1.953rem;
  letter-spacing: 0px;
}

h4,
.h4 {
  font-size: 1.563rem;
  letter-spacing: 0.25px;
}

h5,
.h5 {
  font-size: 1.25rem;
  letter-spacing: 0px;
}

a,
p {
  font-family:  var(--bodyFontFamily);
  font-size: var(--bodyFontSize);
  font-weight: var(--fwRegular);
  letter-spacing: 0.5px;
  line-height: 1.8;
  margin: calc(0.5 * var(--spacing)) 0 0;
}

a {
  color: var(--blackMain);
  text-decoration: none;
  &:active, .active {
    color: var(--logoMain);
}
  &:hover {
    color: var(--secondaryMain);
  }
}
`;
