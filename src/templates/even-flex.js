import React from "react";
import styled from "styled-components";

/**
 * Creates a flex container and evenly distributes all children
 * provided.
 *
 * @param {object} props The props passed to the component
 * @param {string} backgroundColor The background color of the column
 * @param {JSX.Element} children The children to flex evenly
 * @param {number | string} width The width of the column (default 100%)
 */
export const EvenFlex = ({ backgroundColor, children, width = "100%" }) => {
  return (
    <FlexWrapper backgroundColor={backgroundColor} width={width}>
      {children}
    </FlexWrapper>
  );
};

const FlexWrapper = styled.div`
  background-color: ${({ backgroundColor }) =>
    backgroundColor ?? "var(--gray)"};
  min-height: 100%;
  display: flex;
  flex-direction: column;
  width: ${({ width }) => (Number.isInteger(width) ? `${width}px` : width)};

  > div {
    flex: 1;
    display: flex;
    flex-direction: column;
    margin: 16px;
  }
`;
