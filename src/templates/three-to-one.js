import React from "react";
import styled from "styled-components";

/**
 * Creates a vertical column with a 3:1 ratio
 *
 * @param {object} props
 * @param {string} backgroundColor The background color of the column
 * @param {JSX.Element} children The components to render. Note, due to the nature of this
 *                               layout only the first two components/divs are given a flex
 * @param {number} width The width of the column
 */
export const ThreeToOne = ({ backgroundColor, children, width = 300 }) => {
  return (
    <FlexWrapper backgroundColor={backgroundColor} width={width}>
      {children}
    </FlexWrapper>
  );
};

const FlexWrapper = styled.div`
  background-color: ${({ backgroundColor }) =>
    backgroundColor ?? "var(--gray)"};
  min-height: 100%;
  display: flex;
  flex-direction: column;
  width: ${({ width }) => width}px;
  padding: 16px;

  > div {
    display: flex;
    flex-direction: column;
  }

  div:nth-child(1) {
    flex: 3;
  }

  div:nth-child(2) {
    flex: 1;
    margin-top: 32px;
  }
`;
