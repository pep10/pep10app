import React from "react";
import {
  createHistory,
  createMemorySource,
  LocationProvider,
} from "@reach/router";

import { MainRouter } from "./routes";
import { SelectedModeContextProvider } from "./contexts";

const source = createMemorySource("/");
const history = createHistory(source);

function App() {
  return (
    <LocationProvider history={history}>
      <SelectedModeContextProvider>
        <MainRouter path="/" />
      </SelectedModeContextProvider>
    </LocationProvider>
  );
}

export default App;
