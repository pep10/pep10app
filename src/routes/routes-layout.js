import React from "react";
import { Router } from "@reach/router";
import styled from "styled-components";

import { PepEditor } from "../editor";
import { IconBar, HelpLeft, Debug, Figures } from "../views";

/**
 * The `MainRouter` takes care of displaying proper containers
 * based on the route
 */
const MainRouter = () => {
  return (
    <Grid>
      <Router primary={false}>
        <IconBar path="/*" />
      </Router>
      <Router primary={false}>
        <Debug path="/bug" />
        <HelpLeft path="/help" />
        <NoWidth path="/" />
      </Router>
      <Router>
        <PepEditor path="/*" />
      </Router>
      <Router primary={false}>
        <Figures path="/*" />
      </Router>
    </Grid>
  );
};

export default MainRouter;

// Takes care of main page layout
const Grid = styled.div`
  display: grid;
  grid-template-columns: 60px auto 1fr auto;
  height: 100vh;
  width: 100%;
`;

// NoWidth is used to collapse columns
const NoWidth = styled.div`
  width: 0px;
`;
